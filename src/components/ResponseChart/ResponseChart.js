import s from './ResponseChart.css';
import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Link from '../Link';
import {Bar as BarChart} from 'react-chartjs';
import ForceGraph from '../ForceGraph';

class ResponseChart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {htag: null, atag: null, force: null};
  }

  componentWillMount(){
    function status(response) {  
      if (response.status >= 200 && response.status < 300) {  
        return Promise.resolve(response)  
      } else {  
          return Promise.reject(new Error(response.statusText))  
      }  
    }

    function json(response) {  
      return response.json()  
    }
    var that = this;
      fetch('http://localhost:3001/user/'+ that.props.user+'/forcegraph').then(status)
      .then(json)
      .then(function(data){
        var data3 = data.data;
        //console.log('Request succeeded with JSON response for data3', data3);
        //console.log('data content ',data3.length);     
        that.setState({force:data3});
      })
    .catch(function(error) {  
      console.log('Request failed', error);  
    });
  }

  componentDidMount() {    
    var that = this;

    function getTagFreq(user,tag){      
    }
    var a = getTagFreq(this.props.user,"@");

    //this.setState({atag:a});
    function status(response) {  
      if (response.status >= 200 && response.status < 300) {  
        return Promise.resolve(response)  
      } else {  
          return Promise.reject(new Error(response.statusText))  
      }  
    }

    function json(response) {  
      return response.json()  
    }

    fetch('http://localhost:3001/user/'+ this.props.user +'/stats/@').then(status)  
    .then(json)  
    .then(function(data) {  
      //console.log('Request succeeded with JSON response', data);
      //console.log('data content ',data.data.length);
      if(data.data.length < 3){ that.setState({atag:data2}); return;}
      var data2 = fillGraphData(data.data);
      if(data2)console.log(data2);
      that.setState({atag:data2});
    })
    .then(function(data){
      fetch('http://localhost:3001/user/'+ that.props.user +'/stats/%23').then(status)  
      .then(json)  
      .then(function(data) {  
        //console.log('Request succeeded with JSON response', data);
        //console.log('data content ',data.data.length);
        if(data.data.length < 3){ that.setState({htag:data2}); return;}
        var data2 = fillGraphData(data.data);
        if(data2)console.log(data2);
        that.setState({htag:data2});
      })
    })
    .catch(function(error) {  
      console.log('Request failed', error);  
    });


    function fillGraphData(data){
      let job = JSON.parse(data);
      console.log("job "+job);
        if(job.length == 0) { return null;}
        var data2 = {
          labels: [],
          datasets: [
              {
                  label: "My First dataset",
                  fillColor: "rgba(220,220,220,0.2)",
                  strokeColor: "rgba(220,220,220,1)",
                  pointColor: "rgba(220,220,220,1)",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(220,220,220,1)",
                  data: []
              }
          ]};
        job.forEach(function(element) {
          data2.labels.push(element.Key);
          data2.datasets[0].data.push(element.cnt);
        });
        return data2;
    }  

  }

  render() {
    var a = <div></div>
    var b = <div></div>
    var c = <div></div>

    var chartOptions = {
        title: {
            display: 'true',
            text: "Aasdf",
            fontSize: 20
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        }
    };
    
    if(this.state.atag != null){      
      a = <div className={s.item}>
      <label className={s.label + ' ' +  s.item}>@ tag rate in past 200 tweets</label>
      <BarChart data={this.state.atag} options={this.chartOptions} width="600" height="250"/>
      </div>;
    }else{
      a = <p>No @[someone] was used.</p>
    }
    if(this.state.htag != null){
      b = <div className={s.item}>
      <label className={s.label + ' ' +  s.item}># tag rate in past 200 tweets</label>
      <BarChart data={this.state.htag} options={this.chartOptions} width="600" height="250"/>
      </div>;
    }else{
      b = <p>No #[something] was used.</p>
    }
    if(this.state.force != null){
      c = 
      <div className={s.item}>
      <label className={s.label + ' ' +  s.item}>Force graph made from past 200 tweets</label>
      <ForceGraph data={this.state.force}/>
      </div>
    }else{
      c = <p>Tweets could not be parsed.</p>
    }
    return (
      <div className={s.center}>
      {a}
      {b}
      {c}
      </div>
    );
  }
}

export default withStyles(s)(ResponseChart);