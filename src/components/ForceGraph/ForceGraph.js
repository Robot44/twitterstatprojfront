import s from './ForceGraph.css';
import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import * as d3 from 'd3';
import { InteractiveForceGraph, ForceGraphNode, ForceGraphLink } from 'react-vis-force';

class ForceGraph extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let resp = null;
    var arr = JSON.parse(this.props.data);
    var red = [];
    var rs = 0;
    var bs = 0;
    var blue = [];
    var links = [];
    arr.forEach(function(a){
        var r = rs;
        var b = bs;
        if(red[a.governorGloss] == undefined){
            red[a.governorGloss] = rs;
            rs+=1;
        }else{
            r = red[a.governorGloss];
        }
        if(blue[a.dependentGloss] == undefined){
            blue[a.dependentGloss] = bs;
            bs+=1;
        }else{
            b = blue[a.dependentGloss];
        }
        links.push(['rr'+r,'bb'+b]);
    });
    var r = [];
    var b = [];
    var l = [];
    for(var index in red){
	    console.log(index + " r " + red[index])
        r.push(<ForceGraphNode node={{ id:'rr'+red[index], label: index }} fill="red" />)
    }
    for(var index in blue){
        console.log(index + " b " + blue[index])
        b.push(<ForceGraphNode node={{ id:'bb'+blue[index], label: index }} fill="blue" />)
    }
    links.forEach(function(element){
        console.log(element[0]+" "+element[1])
        l.push(<ForceGraphLink link={{ source: element[0], target: element[1] }} />)
    });
    return (
      <div >
        <InteractiveForceGraph class="graph"
        simulationOptions={{ height: 300, width: 300 }}
        labelAttr="label"
        onSelectNode={(node) => console.log(node)}
        highlightDependencies
        >
        {r}
        {b}
        {l}
        </InteractiveForceGraph>   
      </div>
    );
  }
}

export default withStyles(s)(ForceGraph);