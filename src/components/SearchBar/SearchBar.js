import s from './SearchBar.css';
import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import SearchResponse from '../SearchResponse';

class SearchBar extends React.Component {

  static propTypes = {
    title: PropTypes.string
  };
  constructor(props) {
    super(props);
    this.state = {value: '', items: null};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value, items : null});
  }

  handleSubmit(event) {
    fetch('http://localhost:3001/user/'+this.state.value,{
      method: 'get',
      dataType: 'json',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })
    .then((response) => 
    {
      if (!response.ok) {
            throw Error(response.statusText);
      }
      return response.json()
    })
    .then((responseData) => {
      this.setState({items:responseData});
    })
    .catch((error) => {
        console.error(error);
    });
    event.preventDefault();
  }

  render() {
    const { title } = this.props;
    let resp = null;
    if(this.state.items == null){
      resp = <div></div>
    }else{
      resp = <SearchResponse data={this.state.items} user={this.state.value}/>
    }
    return (
      <div className={s.center}>
        <form  onSubmit={this.handleSubmit}>
          <input type="text" value={this.state.value} onChange={this.handleChange} />
          <input type="submit" value="Search" />
        </form>
        {resp}
      </div>
    );
  }
}

export default withStyles(s)(SearchBar);