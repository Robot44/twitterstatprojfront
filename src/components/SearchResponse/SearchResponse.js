import s from './SearchResponse.css';
import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Link from '../Link';
import ResponseChart from '../ResponseChart';

class SearchResponse extends React.Component {

  static propTypes = {
    data: PropTypes.object,
    user: PropTypes.string,
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    
  }

  render() {
    let resp = null;
    console.log(this.props.data.exist);
    if(this.props.data.exist){
      resp = 
      <div>
      <ResponseChart user={this.props.user} />
      </div>
    }else{
      resp = 
      <div>
      <p>User not found. Search for it by authorising us to make twitter api calls on your behalf with fallowing link</p>
      <a href={"http://localhost:3001/sessions/connect/"}>Connect</a>
      </div>;
    }

    return (
      <div className={s.center}>
      {resp}
      </div>
    );
  }
}

export default withStyles(s)(SearchResponse);