import s from './UserLogin.css';
import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

class UserLogin extends React.Component {

  static propTypes = {
    data: PropTypes.object,
    user: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.state = {userOn: false, username:''};
    this.handleLogout = this.handleLogout.bind(this);
  }

  componentDidMount(){
    fetch('http://localhost:3001/sessions/info/',{
        method: 'get',
        dataType: 'json',
        headers: {
        'Accept': "text/plain",
        'Content-Type': "text/plain"
      },
      credentials: 'include'
    })
    .then((response) => 
    {
      if (!response.ok) {
            throw Error(response.statusText);
      }
      return response.text()
    })
    .then((responseData) => {
      console.log('ersppp '+ JSON.stringify(responseData));
      this.setState({userOn: true, username: responseData});
    })
    .catch((error) => {
        console.error(error);
    });
  }

  handleLogout() {
    fetch('http://localhost:3001/sessions/end/',{
      method: 'get',
      dataType: 'json',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })
    .then((response) => 
    {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      //return response.json()
      window.location.reload();
    })
    .catch((error) => {
        console.error(error);
    });
  }

  render() {
    let resp = null;
    if(this.state.userOn){
      resp =  
      <div>
        <p>{'User: '+ this.state.username}</p>
        <button onClick={this.handleLogout}>
          Logout
        </button>
      </div>
    }
    return (
      <div >
      {resp}
      </div>
    );
  }
}

export default withStyles(s)(UserLogin);