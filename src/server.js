/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import 'babel-polyfill';
import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import expressJwt from 'express-jwt';
import expressGraphQL from 'express-graphql';
import jwt from 'jsonwebtoken';
import React from 'react';
import ReactDOM from 'react-dom/server';
import UniversalRouter from 'universal-router';
import PrettyError from 'pretty-error';
import App from './components/App';
import Html from './components/Html';
import { ErrorPageWithoutStyle } from './routes/error/ErrorPage';
import errorPageStyle from './routes/error/ErrorPage.css';
import passport from './core/passport';
import models from './data/models';
import schema from './data/schema';
import routes from './routes';
import assets from './assets'; // eslint-disable-line import/no-unresolved
import { port, auth } from './config';
import session from 'express-session';
import twitter from 'twitter';
import { auth as config } from './config';
import oauth from 'oauth';
import util from 'util';
import inspect from 'util-inspect';
import request from 'request';

const app = express();

//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

//
// Register Node.js middleware
// -----------------------------------------------------------------------------

var expiryDate = new Date(Date.now() + 60 * 60 * 1000) // 1 hour
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(session({ secret: "very secret", resave: false, saveUninitialized: true}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//
// Authentication
// -----------------------------------------------------------------------------

app.use(function(req, res, next) {
  res.locals.session = req.session;
  next();
});

var consumer = new oauth.OAuth(
    "https://twitter.com/oauth/request_token",
    "https://twitter.com/oauth/access_token", 
    config.twitter.key,
    config.twitter.secret,
    "1.0A",
    "http://localhost:3001/sessions/callback/",
    "HMAC-SHA1"
);

app.get('/sessions/connect/', function(req, res){
  console.log("executed connect");
  consumer.getOAuthRequestToken(function(error, oauth_token, oauth_token_secret, results){
		if (error) {
			console.log(error);
			res.send("yeah no. didn't work.")
		}
		else {
			req.session.oauth = {};
			req.session.oauth.token = oauth_token;
			console.log('oauth.token: ' + req.session.oauth.token);
			req.session.oauth.token_secret = oauth_token_secret;
			console.log('oauth.token_secret: ' + req.session.oauth.token_secret);
			res.redirect('https://twitter.com/oauth/authenticate?oauth_token='+oauth_token)
	}
	});
});

app.get('/sessions/callback/', function(req, res){
  if (req.session.oauth) {
		req.session.oauth.verifier = req.query.oauth_verifier;
		var oauth = req.session.oauth;
		consumer.getOAuthAccessToken(oauth.token,oauth.token_secret,oauth.verifier, 
      function(error, oauth_access_token, oauth_access_token_secret, results){
        if (error){
          console.log(error);
          res.send("yeah something broke.");
        } else {
          req.session.oauth.access_token = oauth_access_token;
          req.session.oauth.access_token_secret = oauth_access_token_secret;
          req.session.oauth.result = results;
          //console.log('callbakc results !! : '+results);
          res.redirect('/');
        }
      }
		);
	} else
		next(new Error("you're not supposed to be here."))
});

app.get('/sessions/end', function(req, res){
  if(req.session != null)req.session.destroy();
  console.log('executed logout');
  console.log(req.session);
  if(req.session == null)res.redirect('/');//status(204).send('No session set');
  else res.status(500).send('Failed to unset session');
});

app.get('/sessions/info', function(req, res){
  console.log('returns session info');
  console.log(req.session)
  console.log(req.session.oauth);
  if(req.session.oauth != null && req.session.oauth != undefined){
    var info = req.session.oauth.result.screen_name;
    console.log('info: '+info);
    res.status(200).send(info);
  }
  else res.status(500).send('No user is on');
});
//
//TwitterStatProj Authentification
//------------------------------------------------------------------------------
const credentials = {
  client: {
    id: 'client',
    secret: 'secret'
  },
  auth: {
    tokenHost: 'http://localhost:57967',
    tokenPath: 'http://localhost:57967/connect/token',
    authorizePath: 'http://localhost:57967/connect/authorize',
  }
};
const oauth2 = require('simple-oauth2').create(credentials);
const tokenConfig = {};
var ares = null;
var token = '';
app.use(function (req, res, next) {
  if(ares!=null)
  var dt = new Date(ares.expires_at);
  var now = new Date(Date.now());
  if(ares == null || dt < now || res.statusCode == 401)
  oauth2.clientCredentials.getToken(tokenConfig, (error, result) => {
    if (error) {
      return console.log('Access Token Error', error.message);
    }else{
      ares = result;
    }
    token = 'Bearer '.concat(result.access_token);
  });
  next()
})
//
// MyApi
//------------------------------------------------------------------------------

app.get('/clearSessions', function(req, res){
  res.send('done ' + JSON.stringify(req.session));
    //req.session.destroy();
    
})

app.get('/user/:id', function(req, res){
  var sess = req.session;
  request({url: 'http://localhost:63618/api/User/'+req.params.id, headers: {'Authorization':token}}
    , function (error, response, body) {
    var loggedin = Boolean(req.session.oauth);
    if (!error && response.statusCode == 200) {
      res.json({'exist':true});
    }else{
      console.log("session is : "+JSON.stringify(sess));
      if(loggedin){
        console.log('is logged in');
        consumer.get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name="+req.params.id+"&count=20"
        , req.session.oauth.access_token, req.session.oauth.access_token_secret, function (error, data, response) {
          if (error) {
            req.session.destroy();
            res.json({'exist':false});
          } else {
            var parsedData = JSON.parse(data);
            var nList = [];
            parsedData.forEach(function(element) {
              nList.push({"text":element.text, "postedAt":new Date(element.created_at)});
            }, this);

            request.post({url: 'http://localhost:63618/api/User/'
            , headers: {'Authorization':token}
            , method: 'post'
            , body: {"Key":req.params.id}
            , json: true,}
            ,function(error, response, body){
              if(error){
                console.log(error);
                res.json({'exist':false});
              }else if(response.statusCode === 201){
                request.post({url: 'http://localhost:63618/api/User/'+req.params.id+'/Post'
                , headers: {'Authorization':token}
                , method: 'post'
                , body: nList
                , json: true,}
                ,function(error, response, body){
                    res.json({'exist':true});
                }
                );
              }
            });
          } 
        });
      }else{
         res.json({'exist':false});
      }
    }
  });
});

app.get('/user/:id/stats/:tag', function(req, res){
  var uri = 'http://localhost:63618/api/User/'+req.params.id+'/twitter/TagFrequency/'+encodeURIComponent(req.params.tag);
  request({url: uri, headers: {'Authorization':token}},
    function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.json({'data':body});
    }else{
      res.json({'data':''});
    }
  });
});

app.get('/user/:id/forcegraph', function(req, res){

  var resp = '[{"dep":"amod","governor":2,"governorGloss":"Author","dependent":1,"dependentGloss":"Renowned"},{"dep":"amod","governor":4,"governorGloss":"agenda","dependent":3,"dependentGloss":"undisclosed"},{"dep":"amod","governor":10,"governorGloss":"movement","dependent":8,"dependentGloss":"implemented"},{"dep":"amod","governor":10,"governorGloss":"movement","dependent":9,"dependentGloss":"environmental"},{"dep":"amod","governor":3,"governorGloss":"@PrisonPlanet","dependent":1,"dependentGloss":"#tcot"},{"dep":"amod","governor":10,"governorGloss":"WO","dependent":8,"dependentGloss":"correct"},{"dep":"amod","governor":53,"governorGloss":"Arpaio","dependent":50,"dependentGloss":"Russian"},{"dep":"amod","governor":57,"governorGloss":"risk","dependent":56,"dependentGloss":"huge"},{"dep":"amod","governor":4,"governorGloss":"Victory","dependent":3,"dependentGloss":"Historic"}]';
  //res.json({'data':resp});
  console.log("executed forcegraph ?@!!?!!")
  var uri = 'http://localhost:63618/api/User/'+req.params.id+'/Twitter/dep/amod';
  request({url: uri, headers: {}},
    function(error, response, body){
      if(!error && response.statusCode == 200){
        console.log("as "+body);
          res.json({'data':body});
        }else{
          res.json({'data':''});
        }
    });
});
//
// Register API middleware
// -----------------------------------------------------------------------------
app.use('/graphql', expressGraphQL(req => ({
  schema,
  graphiql: process.env.NODE_ENV !== 'production',
  rootValue: { request: req },
  pretty: process.env.NODE_ENV !== 'production',
})));

//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
app.get('*', async (req, res, next) => {
  try {
    const css = new Set();

    // Global (context) variables that can be easily accessed from any React component
    // https://facebook.github.io/react/docs/context.html
    const context = {
      // Enables critical path CSS rendering.
      // https://github.com/kriasoft/isomorphic-style-loader
      insertCss: (...styles) => {
        // eslint-disable-next-line no-underscore-dangle
        styles.forEach(style => css.add(style._getCss()));
      },
    };

    const route = await UniversalRouter.resolve(routes, {
      path: req.path,
      query: req.query,
    });

    if (route.redirect) {
      res.redirect(route.status || 302, route.redirect);
      return;
    }

    const data = { ...route };
    data.children = ReactDOM.renderToString(<App context={context}>{route.component}</App>);
    data.style = [...css].join('');
    data.script = assets.client.js;
    data.chunk = assets[route.chunk] && assets[route.chunk].js;
    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);

    res.status(route.status || 200);
    res.send(`<!doctype html>${html}`);
  } catch (err) {
    next(err);
  }
});

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  console.log(pe.render(err)); // eslint-disable-line no-console
  const html = ReactDOM.renderToStaticMarkup(
    <Html
      title="Internal Server Error"
      description={err.message}
      style={errorPageStyle._getCss()} // eslint-disable-line no-underscore-dangle
    >
      {ReactDOM.renderToString(<ErrorPageWithoutStyle error={err} />)}
    </Html>,
  );
  res.status(err.status || 500);
  res.send(`<!doctype html>${html}`);
});

//
// Launch the server
// -----------------------------------------------------------------------------
/* eslint-disable no-console */
models.sync().catch(err => console.error(err.stack)).then(() => {
  app.listen(port, () => {
    console.log(`The server is running at http://localhost:${port}/`);
  });
});
/* eslint-enable no-console */
