/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/* eslint-disable max-len */

export const port = process.env.PORT || 3000;
export const host = process.env.WEBSITE_HOSTNAME || `localhost:${port}`;

export const databaseUrl = process.env.DATABASE_URL || 'sqlite:database.sqlite';

export const auth = {

  jwt: { secret: process.env.JWT_SECRET || 'SomethingSomethingSecret' },

  // https://apps.twitter.com/
  twitter: {
    key: process.env.TWITTER_CONSUMER_KEY || 'CF7jstbzfCl5TjH0uT96Rq7lb',
    secret: process.env.TWITTER_CONSUMER_SECRET || 'LrsnRqZ9EM7Tgylb1IxXTka9li6oumtXOK5auFM92hatsogpZg',
  },

};
