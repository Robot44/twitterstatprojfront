/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import fetch from '../../core/fetch';
import Layout from '../../components/Layout';
import SearchBar from '../../components/SearchBar';

const title = 'Search';

export default {

  path: '/',

  children: [
    {
      path: '/',
      action() {
        return {
          title,
          component: <Layout><SearchBar title={title}></SearchBar></Layout>,
        };
      },
    },
    {
      path: '/:user',
      action({params}) {
        return {
          title,
          component: <Layout><SearchBar title={title} user={params.user}></SearchBar></Layout>,
        };
      },
    }
  ]

};
